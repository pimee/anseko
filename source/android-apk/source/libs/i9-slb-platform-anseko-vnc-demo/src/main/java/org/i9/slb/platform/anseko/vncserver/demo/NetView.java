package org.i9.slb.platform.anseko.vncserver.demo;

import org.i9.slb.platform.anseko.libs.net.client.LogPanel;
import org.i9.slb.platform.anseko.libs.net.logging.VLogger;
import org.i9.slb.platform.anseko.libs.net.server.VNCServer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

public class NetView extends Applet implements LogPanel {
    private static final long serialVersionUID = -573115987180419237L;
    public static NetView instance = null;
    protected String startText;
    protected String stopText;

    protected String hostName;
    protected JButton actionButton;

    protected Map params;
    protected boolean autoStart;
    protected boolean reverseMode;
    protected int port = 5500;
    protected String mapid;
    protected String address;
    protected String password;
    private VNCServer vnc;

    public NetView() {
        startText = "Start";
        stopText = "Stop";
        actionButton = new JButton(startText);

        autoStart = false;
        reverseMode = false;
        port = 5500;
        vnc = new VNCServer();

    }

    private void startGUI() {
        Runnable runnable = new Runnable() {
            public void run() {
                vnc.start(address, port, mapid);
                actionButton.setLabel(stopText);
                actionButton.setEnabled(true);
            }
        };

        SwingUtilities.invokeLater(runnable);
    }

    private void stopGUI() {
        Runnable runnable = new Runnable() {
            public void run() {
                if (vnc != null)
                    vnc.close();
                actionButton.setLabel(startText);
                actionButton.setEnabled(true);
            }

        };

        SwingUtilities.invokeLater(runnable);
    }

    public void init() {
        instance = this;
        try {
            InetAddress inetaddress = InetAddress.getLocalHost();
            hostName = inetaddress.getHostName();
        } catch (UnknownHostException unknownhostexception) {
            unknownhostexception.printStackTrace();
        }
        VLogger.setLogPanel(this);

        Object obj = null;

        String s1 = getParameter("port");
        if (s1 != null)
            try {
                port = Integer.parseInt(s1);
            } catch (Exception exception6) {
            }
        else if ("true".equalsIgnoreCase(getParameter("autoStart")))
            autoStart = true;
        else
            autoStart = false;

        this.resize(500, 300);
    }

    public void start() {
        Object obj;
        for (obj = this; ((Container) (obj)).getParent() != null && !(obj instanceof Frame); obj = ((Container) (obj)).getParent())
            ;
        if (!(obj instanceof Frame))
            obj = null;

        JPanel pane = new JPanel(new GridBagLayout());
        pane.setBackground(Color.WHITE);

        pane.setBorder(BorderFactory.createTitledBorder(
                null, "Reverse Connect VNC Viewer",
                TitledBorder.LEADING, TitledBorder.TOP,
                new Font("Dialog", Font.ITALIC, 14),
                Color.RED)
        );

        add(pane);
        int PADDING = 4;

        setLayout(new GridBagLayout());

        //=========NEW UI==================================================
        GridBagConstraints cServerLabel = new GridBagConstraints();
        cServerLabel.gridx = 0;
        cServerLabel.gridy = 2;
        cServerLabel.weightx = 100;
        cServerLabel.weighty = 100;
        cServerLabel.gridwidth = 1;
        cServerLabel.gridheight = 1;
        cServerLabel.anchor = GridBagConstraints.LINE_END;
        cServerLabel.ipadx = PADDING;
        pane.add(new JLabel("  IP Address:"), cServerLabel);

        //final JTextField serverNameField = new JTextField(null == serverNameString ? "" : serverNameString, 20);
        final JTextField serverNameField = new JTextField("10.101.44.128", 20);
        GridBagConstraints cServerName = new GridBagConstraints();
        cServerName.gridx = 1;
        cServerName.gridy = 2;
        cServerName.weightx = 0;
        cServerName.weighty = 100;
        cServerName.gridwidth = 1;
        cServerName.gridheight = 1;
        cServerName.anchor = GridBagConstraints.LINE_START;
        pane.add(serverNameField, cServerName);

        GridBagConstraints cPortLabel = new GridBagConstraints();
        cPortLabel.gridx = 0;
        cPortLabel.gridy = 1;
        cPortLabel.weightx = 100;
        cPortLabel.weighty = 100;
        cPortLabel.gridwidth = 1;
        cPortLabel.gridheight = 1;
        cPortLabel.anchor = GridBagConstraints.LINE_END;
        cPortLabel.ipadx = PADDING;
        cPortLabel.ipady = 10;
        pane.add(new JLabel("Port:"), cPortLabel);

        GridBagConstraints cPort = new GridBagConstraints();
        cPort.gridx = 1;
        cPort.gridy = 1;
        cPort.weightx = 0;
        cPort.weighty = 100;
        cPort.gridwidth = 1;
        cPort.gridheight = 1;
        cPort.anchor = GridBagConstraints.LINE_START;
        final JTextField serverPortField = new JTextField("5999", 10);
        pane.add(serverPortField, cPort);

        GridBagConstraints cCusidLabel = new GridBagConstraints();
        cCusidLabel.gridx = 0;
        cCusidLabel.gridy = 0;
        cCusidLabel.weightx = 100;
        cCusidLabel.weighty = 100;
        cCusidLabel.gridwidth = 1;
        cCusidLabel.gridheight = 1;
        cCusidLabel.anchor = GridBagConstraints.LINE_END;
        cCusidLabel.ipadx = PADDING;
        cCusidLabel.ipady = 10;
        pane.add(new JLabel("ID:"), cCusidLabel);

        GridBagConstraints cidNumber = new GridBagConstraints();
        cidNumber.gridx = 1;
        cidNumber.gridy = 0;
        cidNumber.weightx = 0;
        cidNumber.weighty = 100;
        cidNumber.gridwidth = 1;
        cidNumber.gridheight = 1;
        cidNumber.anchor = GridBagConstraints.LINE_START;
        final JTextField cusidField = new JTextField("888888", 10);
        pane.add(cusidField, cidNumber);

        JPanel buttonPanel = new JPanel();

        buttonPanel.setBackground(Color.WHITE);

        buttonPanel.add(actionButton);
        actionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Object obj = e.getSource();
                if (obj == actionButton) {
                    if (startText.equals(actionButton.getLabel())) {

                        String s = serverNameField.getText();
                        if (s != null && s.trim().length() > 0)
                            address = s.trim();

                        try {
                            port = Integer.parseInt(serverPortField.getText());
                            mapid = cusidField.getText();
                        } catch (Exception exception) {
                        }
                        startGUI();
                    } else if (stopText.equals(actionButton.getLabel()))
                        stopGUI();
                } else {
                    System.out.println(obj);
                }

            }
        });

        JButton closeButton = new JButton("Close");
        buttonPanel.add(closeButton);
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);

            }
        });

        GridBagConstraints cButtons = new GridBagConstraints();
        cButtons.gridx = 0;
        cButtons.gridy = 3;
        cButtons.weightx = 100;
        cButtons.weighty = 100;
        cButtons.gridwidth = 2;
        cButtons.gridheight = 1;
        pane.add(buttonPanel, cButtons);

    }

    public void destroy() {
        super.destroy();
        if (vnc != null)
            vnc.close();
    }

    @SuppressWarnings("unused")
    private String formatMessage(String s, String s1) {
        return s + s1;
    }

    public void showMsg(String s, String s1, int i) {

    }

}
