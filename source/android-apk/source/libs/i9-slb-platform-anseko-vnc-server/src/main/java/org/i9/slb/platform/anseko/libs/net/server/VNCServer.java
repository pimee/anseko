package org.i9.slb.platform.anseko.libs.net.server;

import org.i9.slb.platform.anseko.libs.net.client.VNCRobot;
import org.i9.slb.platform.anseko.libs.net.rfb.server.NoAuthenticator;

public class VNCServer {
    VNCHost host;

    public void start() {
        start(null, 5900, "Server");
    }

    public void start(String address, int port, String mappid) {
        NoAuthenticator localNoAuthenticator = new NoAuthenticator();
        Class localClass = VNCRobot.class;
        int i = port - 5500;
        try {
            this.host = new VNCHost(i, address, localClass, localNoAuthenticator, false, mappid);
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    public void close() {
        if (this.host == null)
            return;
        this.host.close();
    }
}

