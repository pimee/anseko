package org.i9.slb.platform.anseko.libs.net.rfb.server;

import org.i9.slb.platform.anseko.libs.net.logging.VLogger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb.server:
//			DesCipher, RFBAuthenticator, RFBSocket, RFBClient

public class DefaultRFBAuthenticator
        implements RFBAuthenticator {

    private int auth;
    String password;
    boolean authSuccessfull;
    String ip;

    public DefaultRFBAuthenticator(String s) {
        password = null;
        authSuccessfull = false;
        ip = null;
        password = s;
        auth = 2;
    }

    public int getAuthScheme(RFBClient rfbclient) {
        return auth;
    }

    public boolean authenticate(DataInputStream datainputstream, DataOutputStream dataoutputstream, RFBSocket rfbsocket)
            throws IOException {
        dataoutputstream.writeInt(2);
        ip = rfbsocket.getInetAddress().getHostAddress();
        VLogger.getLogger().log("MSG1", ip);
        authSuccessfull = enterPassword(datainputstream, dataoutputstream, password);
        if (authSuccessfull) {
            VLogger.getLogger().log("MSG2", ip);
        } else {
            dataoutputstream.writeInt(0);
            dataoutputstream.writeBytes("Security Authentication failed.  You must be logged on to ESPM in order to use this function");
            VLogger.getLogger().log("MSG3", ip);
        }
        dataoutputstream.flush();
        return authSuccessfull;
    }

    public static boolean enterPassword(DataInputStream datainputstream, DataOutputStream dataoutputstream, String s)
            throws IOException {
        Random random = new Random(System.currentTimeMillis());
        byte abyte0[] = new byte[16];
        random.nextBytes(abyte0);
        dataoutputstream.write(abyte0);
        dataoutputstream.flush();
        byte abyte1[] = new byte[16];
        byte abyte2[] = new byte[16];
        datainputstream.read(abyte1);
        DesCipher descipher = new DesCipher(s.getBytes());
        descipher.decrypt(abyte1, 0, abyte2, 0);
        descipher.decrypt(abyte1, 8, abyte2, 8);
        System.out.println("Client sent us:" + new String(abyte2));
        boolean flag = true;
        int i = 0;
        do {
            if (i >= 16)
                break;
            if (abyte0[i] != abyte2[i]) {
                flag = false;
                break;
            }
            i++;
        } while (true);
        if (flag)
            dataoutputstream.writeInt(0);
        else
            dataoutputstream.writeInt(1);
        dataoutputstream.flush();
        return flag;
    }
}
