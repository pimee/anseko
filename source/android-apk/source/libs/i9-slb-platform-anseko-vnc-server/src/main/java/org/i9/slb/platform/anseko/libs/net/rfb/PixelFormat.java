package org.i9.slb.platform.anseko.libs.net.rfb;

import java.awt.image.ColorModel;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.PrintStream;

public class PixelFormat {

    public static final PixelFormat BGR233 = new PixelFormat(8, 8, false, true, 7, 7, 3, 0, 3, 6);
    public static final PixelFormat BGR565 = new PixelFormat(16, 16, false, true, 31, 63, 31, 0, 5, 11);
    public static final PixelFormat RGB888 = new PixelFormat(32, 24, false, true, 255, 255, 255, 16, 8, 0);
    public int bitsPerPixel;
    public int depth;
    public boolean bigEndian;
    public boolean trueColour;
    public int redMax;
    public int greenMax;
    public int blueMax;
    public int redShift;
    public int greenShift;
    public int blueShift;
    private ColorModel colorModel;

    public PixelFormat() {
        redMax = 255;
        greenMax = 255;
        blueMax = 255;
        colorModel = null;
    }

    public PixelFormat(int i, int j, boolean flag, boolean flag1, int k, int l, int i1,
                       int j1, int k1, int l1) {
        redMax = 255;
        greenMax = 255;
        blueMax = 255;
        colorModel = null;
        bitsPerPixel = i;
        depth = j;
        bigEndian = flag;
        trueColour = flag1;
        redMax = k;
        greenMax = l;
        blueMax = i1;
        redShift = j1;
        greenShift = k1;
        blueShift = l1;
    }

    public PixelFormat(DataInput datainput)
            throws IOException {
        redMax = 255;
        greenMax = 255;
        blueMax = 255;
        colorModel = null;
        bitsPerPixel = datainput.readUnsignedByte();
        depth = datainput.readUnsignedByte();
        bigEndian = datainput.readUnsignedByte() != 0;
        trueColour = datainput.readUnsignedByte() != 0;
        redMax = datainput.readUnsignedShort();
        greenMax = datainput.readUnsignedShort();
        blueMax = datainput.readUnsignedShort();
        redShift = datainput.readUnsignedByte();
        greenShift = datainput.readUnsignedByte();
        blueShift = datainput.readUnsignedByte();
    }

    public PixelFormat(PixelFormat pixelformat) {
        redMax = 255;
        greenMax = 255;
        blueMax = 255;
        colorModel = null;
        bitsPerPixel = pixelformat.bitsPerPixel;
        depth = pixelformat.depth;
        bigEndian = pixelformat.bigEndian;
        trueColour = pixelformat.trueColour;
        redMax = pixelformat.redMax;
        greenMax = pixelformat.greenMax;
        blueMax = pixelformat.blueMax;
        redShift = pixelformat.redShift;
        greenShift = pixelformat.greenShift;
        blueShift = pixelformat.blueShift;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        dataoutput.writeByte(bitsPerPixel);
        dataoutput.writeByte(depth);
        dataoutput.writeByte(bigEndian ? 1 : 0);
        dataoutput.writeByte(trueColour ? 1 : 0);
        dataoutput.writeShort(redMax);
        dataoutput.writeShort(greenMax);
        dataoutput.writeShort(blueMax);
        dataoutput.writeByte(redShift);
        dataoutput.writeByte(greenShift);
        dataoutput.writeByte(blueShift);
    }

    public void print(PrintStream printstream) {
        printstream.println("Bits-per-pixel: " + bitsPerPixel);
        printstream.println("Depth:          " + depth);
        printstream.println("Big Endian:     " + bigEndian);
        printstream.println("True Colour:    " + trueColour);
        printstream.println("R max:   " + redMax);
        printstream.println("G max:   " + greenMax);
        printstream.println("B max:   " + blueMax);
        printstream.println("R shift: " + redShift);
        printstream.println("G shift: " + greenShift);
        printstream.println("B shift: " + blueShift);
    }

    public int translatePixel(int i) {
        int j = i & 0xff;
        int k = i >> 8 & 0xff;
        int l = i >> 16 & 0xff;
        int i1 = i >> 24 & 0xff;
        if (colorModel != null)
            try {
                j = colorModel.getBlue(i);
                k = colorModel.getGreen(i);
                l = colorModel.getRed(i);
                i1 = colorModel.getAlpha(i);
            } catch (Exception exception) {
            }
        if (trueColour) {
            j = (j * blueMax + 128) / 255;
            k = (k * greenMax + 128) / 255;
            l = (l * redMax + 128) / 255;
            i = j << blueShift | k << greenShift | l << redShift;
        } else {
            switch (bitsPerPixel) {
                default:
                    break;

                case 32: // ' '
                    if (bigEndian)
                        i = i1 | l << 8 | k << 16 | j << 24;
                    else
                        i = i1 << 24 | l << 16 | k << 8 | j;
                    // fall through

                case 24: // '\030'
                    if (bigEndian)
                        i = l | k << 8 | j << 16;
                    else
                        i = l << 16 | k << 8 | j;
                    break;

                case 16: // '\020'
                    if (bigEndian)
                        i = j >> 3 | (k >> 2) << 5 | (l >> 3) << 11;
                    else
                        i = (j >> 3) << 11 | (k >> 2) << 5 | l >> 3;
                    break;

                case 8: // '\b'
                    i = (j >> 6) << 6 | (k >> 5) << 3 | l >> 5;
                    break;
            }
        }
        return i;
    }

    public ColorModel getColorModel() {
        return colorModel;
    }

    public void setColorModel(ColorModel colormodel) {
        colorModel = colormodel;
    }

    private static int fixColorModel(int i, int j, int k) {
        int l;
        for (l = 0; l < 8 && i != j; l++)
            i >>= 1;

        for (; (k & 1) == 0; k >>= 1)
            l++;

        return l;
    }

    public boolean equal(Object obj) {
        if (obj instanceof PixelFormat) {
            if (obj == this)
                return true;
            PixelFormat pixelformat = (PixelFormat) obj;
            if (bitsPerPixel == pixelformat.bitsPerPixel && depth == pixelformat.depth && bigEndian == pixelformat.bigEndian && trueColour == pixelformat.trueColour && redMax == pixelformat.redMax && greenMax == pixelformat.greenMax && blueMax == pixelformat.blueMax && redShift == pixelformat.redShift && greenShift == pixelformat.greenShift && blueShift == pixelformat.blueShift)
                return true;
        }
        return false;
    }

}
