package org.i9.slb.platform.anseko.libs.net.logging;

import org.i9.slb.platform.anseko.libs.net.client.LogPanel;

public class VLogger {
    private static VLogger instance = null;
    private static LogPanel logPanel = null;

    public static void setLogPanel(LogPanel paramLogPanel) {
        logPanel = paramLogPanel;
    }

    public void log(String paramString1, String paramString2) {
        if (logPanel != null)
            logPanel.showMsg(paramString1, paramString2, 0);
        else
            System.out.println(paramString2);
    }

    public static VLogger getLogger() {
        if (instance == null)
            instance = new VLogger();
        return instance;
    }

    public static void setLogger(VLogger paramVLogger) {
        instance = paramVLogger;
    }
}
