package org.i9.slb.platform.anseko.libs.net.rfb.server;

import org.i9.slb.platform.anseko.libs.net.rfb.Colour;
import org.i9.slb.platform.anseko.libs.net.rfb.PixelFormat;

import java.io.IOException;

public abstract interface RFBServer {
    public abstract void close();

    public abstract void addClient(RFBClient paramRFBClient);

    public abstract void removeClient(RFBClient paramRFBClient);

    public abstract String getDesktopName(RFBClient paramRFBClient);

    public abstract int getFrameBufferWidth(RFBClient paramRFBClient);

    public abstract int getFrameBufferHeight(RFBClient paramRFBClient);

    public abstract PixelFormat getPreferredPixelFormat(RFBClient paramRFBClient);

    public abstract boolean allowShared();

    public abstract void setClientProtocolVersionMsg(RFBClient paramRFBClient, String paramString)
            throws IOException;

    public abstract void setShared(RFBClient paramRFBClient, boolean paramBoolean)
            throws IOException;

    public abstract void setPixelFormat(RFBClient paramRFBClient, PixelFormat paramPixelFormat)
            throws IOException;

    public abstract void setEncodings(RFBClient paramRFBClient, int[] paramArrayOfInt)
            throws IOException;

    public abstract void fixColourMapEntries(RFBClient paramRFBClient, int paramInt, Colour[] paramArrayOfColour)
            throws IOException;

    public abstract void frameBufferUpdateRequest(RFBClient paramRFBClient, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
            throws IOException;

    public abstract void keyEvent(RFBClient paramRFBClient, boolean paramBoolean, int paramInt)
            throws IOException;

    public abstract void pointerEvent(RFBClient paramRFBClient, int paramInt1, int paramInt2, int paramInt3)
            throws IOException;

    public abstract void clientCutText(RFBClient paramRFBClient, String paramString)
            throws IOException;
}
