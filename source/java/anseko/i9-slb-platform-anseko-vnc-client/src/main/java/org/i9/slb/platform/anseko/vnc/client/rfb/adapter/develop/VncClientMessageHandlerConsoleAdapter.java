package org.i9.slb.platform.anseko.vnc.client.rfb.adapter.develop;

import org.apache.log4j.Logger;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.impl.JavaRfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.VncClientMessageHandlerAdapter;
import org.i9.slb.platform.anseko.vnc.client.rfb.connection.ConnectionParameter;
import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.PixelFormat;

/**
 * vnc客户端消息处理命令行（适配器）
 *
 * @author r12
 * @date 2019年1月31日 13:40:49
 */
public class VncClientMessageHandlerConsoleAdapter extends VncClientMessageHandlerAdapter {

    private static final Logger LOGGER = Logger.getLogger(VncClientMessageHandlerConsoleAdapter.class);

    private boolean fullColour, autoSelect, formatChange, encodingChange;

    /**
     * 安全认证成功后调用方法
     */
    @Override
    public void securityAuthSuccess() {
    }

    @Override
    public void setCursor(int hotspotX, int hotspotY, int w, int h, byte[] data, byte[] mask) {
    }

    @Override
    public void serverInit() {
        this.setState(RFBSTATE_NORMAL);
        LOGGER.debug("vnc客户端连接服务器初始化成功");
        if (!getConnectionParameter().getPixelFormat().trueColour) {
            fullColour = true;
        }
        formatChange = encodingChange = true;
        requestNewUpdate();
    }

    @Override
    public void framebufferUpdateStart() {
    }

    @Override
    public void framebufferUpdateEnd() {
        if (autoSelect) {
            autoSelectFormatAndEncoding();
        }
        requestNewUpdate();
    }

    private void autoSelectFormatAndEncoding() {
        JavaRfbInputStream javaRfbInputStream = (JavaRfbInputStream) this.getInputStream();
        long kbitsPerSecond = javaRfbInputStream.kbitsPerSecond();
        int newEncoding = super.getCurrentEncoding();

        if (kbitsPerSecond > 3000) {
            newEncoding = Encodings.HEXTILE;
        } else if (kbitsPerSecond < 1500) {
            newEncoding = Encodings.ZRLE;
        }

        if (newEncoding != super.getCurrentEncoding()) {
            LOGGER.info("Throughput " + kbitsPerSecond + " kbit/s - changing to " + Encodings.valueOf(newEncoding)
                    + " encoding");
            super.setCurrentEncoding(newEncoding);
            this.encodingChange = true;
        }
    }

    private void requestNewUpdate() {
        if (this.formatChange) {
            String str = getConnectionParameter().getPixelFormat().print();
            LOGGER.info("Using pixel format " + str);
            getConnectionParameter().setPixelFormat(new PixelFormat(8, 8, false, false, 0, 0, 0, 0, 0, 0));
            synchronized (this) {
                writer().writeSetPixelFormat(getConnectionParameter().getPixelFormat());
            }
        }
        checkEncodings();
        synchronized (this) {
            ConnectionParameter connectionParameter = getConnectionParameter();
            writer().writeFramebufferUpdateRequest(0, 0, connectionParameter.getWidth(), connectionParameter.getHeight(), !this.formatChange);
        }
        this.formatChange = false;
    }

    @Override
    public void beginRect(int x, int y, int w, int h, int encoding) {
    }

    @Override
    public void endRect(int x, int y, int w, int h, int encoding) {
    }

    @Override
    public void setColourMapEntries(int firstColour, int nColours, int[] rgbs) {
    }

    @Override
    public void bell() {
        LOGGER.info("vnc客户端触发, 播放音乐");
    }

    @Override
    public void serverCutText(String str) {
        LOGGER.info("vnc客户端触发, 复制文本 : " + str);
    }

    @Override
    public void fillRect(int x, int y, int w, int h, int pix) {

    }

    @Override
    public void imageRect(int x, int y, int w, int h, byte[] pix, int offset) {

    }

    @Override
    public void copyRect(int x, int y, int w, int h, int srcX, int srcY) {

    }

    @Override
    public boolean isEncodingChange() {
        return encodingChange;
    }

    @Override
    public void setEncodingChange(boolean encodingChange) {
        this.encodingChange = encodingChange;
    }
}
