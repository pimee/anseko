package org.i9.slb.platform.anseko.vnc.client.rfb.security;

import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.VncClientMessageHandlerAdapter;

/**
 * vnc客户端安全认证
 *
 * @author r12
 * @date 2019年2月1日 10:16:58
 */
public abstract class VncClientSecurity {

    /**
     * 处理安全认证消息
     *
     * @param adapter
     * @return
     */
    abstract public boolean processMessage(VncClientMessageHandlerAdapter adapter);

    /**
     * 返回认证类型
     *
     * @return
     */
    abstract public int getType();
}
