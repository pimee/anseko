package org.i9.slb.platform.anseko.vnc.client.iostream.input.impl;

import org.i9.slb.platform.anseko.vnc.client.exception.IoStreamException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;

import java.util.zip.Inflater;

/**
 * ziplib式InputStream
 *
 * @author jiangtao
 * @date 2019年1月24日 09:35:56
 */
public class ZlibRfbInputStream extends RfbInputStream {

    static final int defaultBufSize = 16384;

    private RfbInputStream underlying;

    private int bufSize;

    private int ptrOffset;

    private Inflater inflater;

    private int bytesIn;

    public ZlibRfbInputStream(int bufSize) {
        this.bufSize = bufSize;
        this.buf = new byte[bufSize];
        this.ptr = this.end = this.ptrOffset = 0;
        this.inflater = new Inflater();
    }

    public ZlibRfbInputStream() {
        this(defaultBufSize);
    }

    public void setUnderlying(RfbInputStream inputStream, int bytesIn) {
        this.underlying = inputStream;
        this.bytesIn = bytesIn;
        this.ptr = this.end = 0;
    }

    public void reset() {
        ptr = end = 0;
        if (underlying == null) {
            return;
        }
        while (bytesIn > 0) {
            decompress();
            end = 0; // throw away any data
        }
        underlying = null;
    }

    @Override
    public int pos() {
        return ptrOffset + ptr;
    }

    @Override
    protected int overrun(int itemSize, int nItems) {
        if (itemSize > bufSize) {
            throw new IoStreamException("ZlibInStream overrun: MAX itemSize exceeded");
        }
        if (underlying == null) {
            throw new IoStreamException("ZlibInStream overrun: no underlying stream");
        }
        if (end - ptr != 0) {
            System.arraycopy(buf, ptr, buf, 0, end - ptr);
        }
        ptrOffset += ptr;
        end -= ptr;
        ptr = 0;

        while (end < itemSize) {
            decompress();
        }

        if (itemSize * nItems > end) {
            nItems = end / itemSize;
        }

        return nItems;
    }

    // decompress() calls the decompressor once.  Note that this won't
    // necessarily generate any output data - it may just consume some input
    // data.  Returns false if wait is false and we would block on the underlying
    // stream.

    private void decompress() {
        try {
            underlying.check(1);
            int availIn = underlying.getEnd() - underlying.getPtr();
            if (availIn > bytesIn) {
                availIn = bytesIn;
            }

            if (inflater.needsInput()) {
                inflater.setInput(underlying.getBuf(), underlying.getPtr(), availIn);
            }

            int n = inflater.inflate(this.buf, end, bufSize - end);

            end += n;
            if (inflater.needsInput()) {
                bytesIn -= availIn;
                underlying.setPtr(underlying.getPtr() + availIn);
            }
        } catch (java.util.zip.DataFormatException e) {
            throw new IoStreamException("ZlibInStream: inflate failed");
        }
    }
}
