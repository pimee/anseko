package org.i9.slb.platform.anseko.common.utils;

import java.util.UUID;

/**
 * uuid生成工具类
 *
 * @author r12
 * @date 2019年2月13日 13:35:19
 */
public class UUIDUtil {

    public static final String generateUUID() {
        return UUID.randomUUID().toString();
    }
}
