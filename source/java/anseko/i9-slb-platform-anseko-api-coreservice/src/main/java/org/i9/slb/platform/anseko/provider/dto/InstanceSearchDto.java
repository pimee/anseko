package org.i9.slb.platform.anseko.provider.dto;

public class InstanceSearchDto implements java.io.Serializable {

    private static final long serialVersionUID = 74227459297069563L;
    /**
     * 起始page
     */
    private int startPage;
    /**
     * 当页总数
     */
    private int pageSize;

    private String instanceName;

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
}
