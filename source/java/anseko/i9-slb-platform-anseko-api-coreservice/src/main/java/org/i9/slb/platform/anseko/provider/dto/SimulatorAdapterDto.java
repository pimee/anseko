package org.i9.slb.platform.anseko.provider.dto;

import java.util.Date;

public class SimulatorAdapterDto implements java.io.Serializable {

    private static final long serialVersionUID = -3563774131896986278L;
    /**
     * 编号
     */
    private String id;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 适配器名称
     */
    private String adapterName;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 更新日期
     */
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getAdapterName() {
        return adapterName;
    }

    public void setAdapterName(String adapterName) {
        this.adapterName = adapterName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
