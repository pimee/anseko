package org.i9.slb.platform.anseko.provider.dto;

/**
 * 模拟器命令组搜索
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class CommandGroupSearchDto implements java.io.Serializable {

    private static final long serialVersionUID = 3733871974563221894L;

    private String simulatorId;

    /**
     * 起始page
     */
    private int startPage;
    /**
     * 当页总数
     */
    private int pageSize;

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
