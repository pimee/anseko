package org.i9.slb.platform.anseko.provider.dto;

import java.util.List;

/**
 * 用户实体类
 *
 * @author jiangtao
 * @date 2019年2月12日 13:42:59
 */
public class UserListDto implements java.io.Serializable {

    private static final long serialVersionUID = 1927731899747634567L;

    private List<UserDto> list;

    private int totalRow;

    public List<UserDto> getList() {
        return list;
    }

    public void setList(List<UserDto> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
