package org.i9.slb.platform.anseko.provider.dto;

import java.util.List;

/**
 * 命令执行器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:52
 */
public class CommandExecuteListDto implements java.io.Serializable {

    private static final long serialVersionUID = -6134205072299629059L;

    private List<CommandExecuteDto> list;

    private int totalRow;

    public List<CommandExecuteDto> getList() {
        return list;
    }

    public void setList(List<CommandExecuteDto> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
