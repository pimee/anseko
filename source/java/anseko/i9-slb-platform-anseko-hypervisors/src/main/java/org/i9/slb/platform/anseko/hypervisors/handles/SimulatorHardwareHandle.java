package org.i9.slb.platform.anseko.hypervisors.handles;

import org.i9.slb.platform.anseko.hypervisors.param.SimulatorDisk;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorNetwork;

/**
 * 虚拟化操作类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 10:48
 */
public abstract class SimulatorHardwareHandle {

    /**
     * 创建物理磁盘
     *
     * @return
     */
    public abstract String[] createDiskPath();

    /**
     * 获取模拟器磁盘
     *
     * @return
     */
    public abstract SimulatorDisk[] buildSimulatorDisks();

    /**
     * 获取模拟器网络
     *
     * @return
     */
    public abstract SimulatorNetwork[] buildSimulatorNetworks();
}
