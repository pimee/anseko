package org.i9.slb.platform.anseko.provider.service.dubbo;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.provider.IDubboVNCServerRemoteService;
import org.i9.slb.platform.anseko.provider.dto.GuacamoleConnectDto;
import org.i9.slb.platform.anseko.provider.entity.InstanceEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorDisplayEntity;
import org.i9.slb.platform.anseko.provider.entity.SimulatorEntity;
import org.i9.slb.platform.anseko.provider.service.InstanceService;
import org.i9.slb.platform.anseko.provider.service.SimulatorDisplayService;
import org.i9.slb.platform.anseko.provider.service.SimulatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * vncserver远程调用服务
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/31 13:48
 */
@Service("dubboVNCServerRemoteService")
public class DubboVNCServerRemoteService implements IDubboVNCServerRemoteService {

    @Autowired
    private SimulatorService simulatorService;

    @Autowired
    private InstanceService instanceService;

    @Autowired
    private SimulatorDisplayService simulatorDisplayService;

    /**
     * 获取模拟器VNC服务信息
     *
     * @param simulatorId
     * @return
     */
    @Override
    public DubboResult<GuacamoleConnectDto> simulatorVNCGuacamoleParam(String simulatorId) {
        DubboResult<GuacamoleConnectDto> dubboResult = new DubboResult<GuacamoleConnectDto>();
        try {
            // 获取模拟器信息
            SimulatorEntity simulatorEntity = this.simulatorService.getSimulatorEntityById(simulatorId);
            // 获取模拟器实例信息
            InstanceEntity instanceEntity = this.instanceService.getInstanceEntityById(simulatorEntity.getInstanceId());

            SimulatorDisplayEntity simulatorDisplayEntity = this.simulatorDisplayService.getSimulatorDisplayEntity(simulatorEntity.getId());

            // 封装Guacamole连接参数
            GuacamoleConnectDto guacamoleConnectDto = new GuacamoleConnectDto();
            guacamoleConnectDto.setProtocol("vnc");
            guacamoleConnectDto.setHostname(instanceEntity.getRemoteAddress());
            guacamoleConnectDto.setPort(simulatorDisplayEntity.getPort());
            guacamoleConnectDto.setPassword(simulatorDisplayEntity.getPassword());
            dubboResult.setRe(guacamoleConnectDto);
        } catch (BusinessException e) {
            dubboResult.setBusinessException(e);
        }
        return dubboResult;
    }
}
