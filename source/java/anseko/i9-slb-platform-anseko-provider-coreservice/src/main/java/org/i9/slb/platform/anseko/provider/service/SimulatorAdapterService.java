package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.dto.SimulatorAdapterDto;
import org.i9.slb.platform.anseko.provider.entity.SimulatorAdapterEntity;
import org.i9.slb.platform.anseko.provider.repository.SimulatorAdapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 模拟器适配器服务类
 *
 * @author r12
 * @date 2019年3月7日 11:02:23
 */
@Service
public class SimulatorAdapterService {

    @Autowired
    private SimulatorAdapterRepository simulatorAdapterRepository;

    /**
     * 创建模拟器适配器
     *
     * @param simulatorId
     * @param simulatorAdapterDtos
     */
    public void createSimulatorAdapter(String simulatorId, List<SimulatorAdapterDto> simulatorAdapterDtos) {
        for (SimulatorAdapterDto simulatorAdapterDto : simulatorAdapterDtos) {
            SimulatorAdapterEntity simulatorAdapterEntity = new SimulatorAdapterEntity();
            simulatorAdapterEntity.setId(UUIDUtil.generateUUID());
            simulatorAdapterEntity.setSimulatorId(simulatorId);
            simulatorAdapterEntity.setAdapterName(simulatorAdapterDto.getAdapterName());
            simulatorAdapterEntity.setCreateDate(new Date());
            simulatorAdapterEntity.setUpdateDate(new Date());
            this.simulatorAdapterRepository.insertSimulatorAdapter(simulatorAdapterEntity);
        }
    }

    /**
     * 获取模拟器适配器
     *
     * @param simulatorId
     * @return
     */
    public List<SimulatorAdapterEntity> getSimulatorAdapterEntityList(String simulatorId) {
        List<SimulatorAdapterEntity> list = this.simulatorAdapterRepository.getSimulatorAdapterEntityList(simulatorId);
        return list;
    }

    /**
     * 复制属性
     *
     * @param simulatorAdapterEntity
     * @param simulatorAdapterDto
     */
    public void copyProperty(SimulatorAdapterEntity simulatorAdapterEntity, SimulatorAdapterDto simulatorAdapterDto) {
        simulatorAdapterDto.setId(simulatorAdapterEntity.getId());
        simulatorAdapterDto.setSimulatorId(simulatorAdapterEntity.getSimulatorId());
        simulatorAdapterDto.setCreateDate(simulatorAdapterEntity.getCreateDate());
        simulatorAdapterDto.setUpdateDate(simulatorAdapterEntity.getUpdateDate());
        simulatorAdapterDto.setAdapterName(simulatorAdapterEntity.getAdapterName());
    }
}
