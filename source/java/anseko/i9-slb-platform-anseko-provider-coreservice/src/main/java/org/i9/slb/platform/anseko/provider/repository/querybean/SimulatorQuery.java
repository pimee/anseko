package org.i9.slb.platform.anseko.provider.repository.querybean;

public class SimulatorQuery extends BasePageQuery implements java.io.Serializable {

    private static final long serialVersionUID = -26021925579256544L;

    private String simulatorName;

    private String instanceId;

    private Integer powerStatus;

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }
}
