package org.i9.slb.platform.anseko.provider.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.provider.entity.SimulatorEntity;
import org.i9.slb.platform.anseko.provider.repository.SimulatorRepository;
import org.i9.slb.platform.anseko.provider.repository.querybean.SimulatorQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SimulatorService {

    @Autowired
    private SimulatorRepository simulatorRepository;

    @Autowired
    private SimulatorDisplayService simulatorDisplayService;

    /**
     * 获取模拟器列表
     *
     * @return
     */
    public List<SimulatorEntity> getSimulatorEntityList() {
        List<SimulatorEntity> list = this.simulatorRepository.getSimulatorEntityList();
        return list;
    }

    /**
     * 获取模拟器信息
     *
     * @param simulatorId
     * @return
     */
    public SimulatorEntity getSimulatorEntityById(String simulatorId) {
        SimulatorEntity simulatorEntity = this.simulatorRepository.getSimulatorEntityById(simulatorId);
        if (simulatorEntity == null) {
            throw new BusinessException("虚拟器不存在");
        }
        return simulatorEntity;
    }

    /**
     * 保存模拟器信息
     *
     * @param simulatorEntity
     */
    public void insertSimulatorEntity(SimulatorEntity simulatorEntity) {
        this.simulatorRepository.insertSimulatorEntity(simulatorEntity);
    }

    /**
     * 删除模拟器
     *
     * @param simulatorId
     */
    public void deleteSimulatorEntity(String simulatorId) {
        this.simulatorRepository.updateSimulatorEntityStatus(simulatorId, 0);
    }

    /**
     * 更新模拟器状态
     *
     * @param simulatorId
     * @param powerState
     */
    public void updateSimulatorEntityPowerState(String simulatorId, int powerState) {
        this.simulatorRepository.updateSimulatorEntityPowerState(simulatorId, powerState);
    }

    /**
     * 查询模拟器列表分页
     *
     * @param simulatorQuery
     * @return
     */
    public PageInfo<SimulatorEntity> getSimulatorEntityList(SimulatorQuery simulatorQuery) {
        PageHelper.startPage(simulatorQuery.getStartPage(), simulatorQuery.getPageSize());
        List<SimulatorEntity> list = this.simulatorRepository.getSimulatorEntityListPage(simulatorQuery);
        PageInfo<SimulatorEntity> pageInfo = new PageInfo<SimulatorEntity>(list);
        return pageInfo;
    }

    /**
     * 获取当前模拟器最大端口
     *
     * @param instanceId
     * @return
     */
    public int getNextSimulatorVNCPortNumber(String instanceId) {
        int port = this.simulatorDisplayService.getNextVNCPort(instanceId);
        return port;
    }

    /**
     * 检查当前模拟器名称是否存在
     *
     * @param simulatorName
     */
    public void checkSimulatorNameAlreadyExist(String simulatorName) {
        int count = this.simulatorRepository.getCountSimulatorNameNumber(simulatorName);
        if (count > 0) {
            throw new BusinessException("error");
        }
    }
}
