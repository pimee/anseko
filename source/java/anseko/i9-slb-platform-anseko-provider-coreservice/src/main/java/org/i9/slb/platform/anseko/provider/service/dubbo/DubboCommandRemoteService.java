package org.i9.slb.platform.anseko.provider.service.dubbo;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.github.pagehelper.PageInfo;
import org.i9.slb.platform.anseko.common.constant.CommandExecuteStatusEnum;
import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.provider.IDubboCommandRemoteService;
import org.i9.slb.platform.anseko.provider.dto.CommandCallbackDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteListDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteSearchDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupListDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupSearchDto;
import org.i9.slb.platform.anseko.provider.entity.CommandExecuteEntity;
import org.i9.slb.platform.anseko.provider.entity.CommandGroupEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.CommandExecuteQuery;
import org.i9.slb.platform.anseko.provider.repository.querybean.CommandGroupQuery;
import org.i9.slb.platform.anseko.provider.service.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 命令处理调度
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:48
 */
@Service("dubboCommandRemoteService")
public class DubboCommandRemoteService implements IDubboCommandRemoteService {

    @Autowired
    private CommandService commandService;

    /**
     * 发起一个命令调试指令
     *
     * @param commandGroupDto
     */
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public DubboResult<?> launchCommandGroup(CommandGroupDto commandGroupDto) {
        // 保存命令执行组
        CommandGroupEntity commandGroupEntity = new CommandGroupEntity();
        commandGroupEntity.setId(UUIDUtil.generateUUID());
        commandGroupEntity.setSimulatorId(commandGroupDto.getSimulatorId());
        commandGroupEntity.setCommandGroupId(commandGroupDto.getCommandGroupId());
        commandGroupEntity.setName(commandGroupDto.getName());
        this.commandService.insertCommandGroupEntity(commandGroupEntity);

        // 如果命令执行集合为空，则不向下处理
        if (CollectionUtils.isEmpty(commandGroupDto.getCommandExecuteDtos())) {
            return DubboResult.ok();
        }

        // 保存命令执行集合
        for (CommandExecuteDto commandExecuteDto : commandGroupDto.getCommandExecuteDtos()) {
            CommandExecuteEntity commandExecuteEntity = new CommandExecuteEntity();
            commandExecuteEntity.setId(UUIDUtil.generateUUID());
            commandExecuteEntity.setCommandGroupId(commandGroupDto.getCommandGroupId());
            commandExecuteEntity.setCommandId(commandExecuteDto.getCommandId());
            commandExecuteEntity.setCommandLine(commandExecuteDto.getCommandLine());
            commandExecuteEntity.setStatus(CommandExecuteStatusEnum.EXECUTING.ordinal());
            this.commandService.insertCommandExecuteEntity(commandExecuteEntity);
        }

        return DubboResult.ok();
    }

    /**
     * 回调命令指令结果
     *
     * @param commandCallbackDto
     */
    @Transactional(rollbackFor = Throwable.class)
    @Override
    public synchronized DubboResult<?> callbackCommandExecute(CommandCallbackDto commandCallbackDto) {
        String commandId = commandCallbackDto.getCommandId();
        String executeResult = commandCallbackDto.getExecuteResult();

        // 如果当前命令已完成，则不进行下面处理
        CommandExecuteEntity commandExecuteEntity = this.commandService.getCommandExecuteEntityListByCommandId(commandId);
        if (commandExecuteEntity.getStatus() == 1) {
            return DubboResult.ok();
        }

        // 更新当前命令状态
        this.commandService.updateCommandExecuteEntityCommandResult(commandId, executeResult);

        // 获取执行组中所有命令判断是否全部完成
        List<CommandExecuteEntity> commandExecuteEntityList = this.commandService.getCommandExecuteEntityListByGroupId(commandExecuteEntity.getCommandGroupId());
        int success = 1;
        for (CommandExecuteEntity commandExecuteEntity0 : commandExecuteEntityList) {
            if (commandExecuteEntity0.getStatus() == 0) {
                success = 0;
                break;
            }
        }

        if (success == 0) {
            return DubboResult.ok();
        }
        this.commandService.updateCommandGroupEndTime(commandExecuteEntity.getCommandGroupId(), success);

        return DubboResult.ok();
    }

    /**
     * 获取模拟器执行命令组列表
     *
     * @param simulatorId
     * @return
     */
    @Override
    public DubboResult<List<CommandGroupDto>> getCommandGroupDtoListSimulatorId(String simulatorId) {
        List<CommandGroupDto> commandGroupDtos = new ArrayList<CommandGroupDto>();
        for (CommandGroupEntity commandGroupEntity : this.commandService.getCommandGroupEntityList(simulatorId)) {
            CommandGroupDto commandGroupDto = new CommandGroupDto();
            commandGroupDto.setCommandGroupId(commandGroupEntity.getCommandGroupId());
            commandGroupDto.setSimulatorId(commandGroupEntity.getSimulatorId());
            commandGroupDto.setStartDate(commandGroupEntity.getStartDate());
            commandGroupDto.setEndDate(commandGroupEntity.getEndDate());
            commandGroupDto.setSuccess(commandGroupEntity.getSuccess());
            commandGroupDtos.add(commandGroupDto);
        }
        DubboResult<List<CommandGroupDto>> dubboResult = new DubboResult<List<CommandGroupDto>>();
        dubboResult.setRe(commandGroupDtos);
        return dubboResult;
    }

    /**
     * 获取模拟器执行命令列表
     *
     * @param commandGroupId
     * @return
     */
    @Override
    public DubboResult<List<CommandExecuteDto>> getCommandExecuteDtosCommandGroupId(String commandGroupId) {
        List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
        for (CommandExecuteEntity commandExecuteEntity : this.commandService.getCommandExecuteEntityListByGroupId(commandGroupId)) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            commandExecuteDto.setCommandGroupId(commandExecuteEntity.getCommandGroupId());
            commandExecuteDto.setCommandId(commandExecuteEntity.getCommandId());
            commandExecuteDto.setCommandLine(commandExecuteEntity.getCommandLine());
            commandExecuteDto.setCommandResult(commandExecuteEntity.getCommandResult());
            commandExecuteDto.setStartDate(commandExecuteEntity.getStartDate());
            commandExecuteDto.setEndDate(commandExecuteEntity.getEndDate());
            commandExecuteDto.setStatus(commandExecuteEntity.getStatus());
            commandExecuteDtos.add(commandExecuteDto);
        }
        DubboResult<List<CommandExecuteDto>> dubboResult = new DubboResult<List<CommandExecuteDto>>();
        dubboResult.setRe(commandExecuteDtos);
        return dubboResult;
    }

    /**
     * 获取模拟器执行命令组列表分页
     *
     * @param commandGroupSearchDto
     * @return
     */
    @Override
    public DubboResult<CommandGroupListDto> getCommandGroupDtoListPage(CommandGroupSearchDto commandGroupSearchDto) {
        CommandGroupQuery commandGroupQuery = new CommandGroupQuery();
        commandGroupQuery.setSimulatorId(commandGroupSearchDto.getSimulatorId());
        commandGroupQuery.setStartPage(commandGroupSearchDto.getStartPage());
        commandGroupQuery.setPageSize(commandGroupSearchDto.getPageSize());

        PageInfo<CommandGroupEntity> pageInfo = this.commandService.getCommandGroupEntityListPage(commandGroupQuery);

        List<CommandGroupDto> list = new ArrayList<CommandGroupDto>();
        for (CommandGroupEntity commandGroupEntity : pageInfo.getList()) {
            CommandGroupDto commandGroupDto = new CommandGroupDto();
            this.copyProperty(commandGroupEntity, commandGroupDto);
            list.add(commandGroupDto);
        }

        CommandGroupListDto commandGroupListDto = new CommandGroupListDto();
        commandGroupListDto.setList(list);
        commandGroupListDto.setTotalRow((int) pageInfo.getTotal());

        DubboResult<CommandGroupListDto> dubboResult = new DubboResult<CommandGroupListDto>();
        dubboResult.setRe(commandGroupListDto);
        return dubboResult;
    }

    private void copyProperty(CommandGroupEntity commandGroupEntity, CommandGroupDto commandGroupDto) {
        commandGroupDto.setCommandGroupId(commandGroupEntity.getCommandGroupId());
        commandGroupDto.setSimulatorId(commandGroupEntity.getSimulatorId());
        commandGroupDto.setStartDate(commandGroupEntity.getStartDate());
        commandGroupDto.setEndDate(commandGroupEntity.getEndDate());
        commandGroupDto.setSuccess(commandGroupEntity.getSuccess());
        commandGroupDto.setName(commandGroupEntity.getName());
    }

    /**
     * 获取模拟器执行命令列表分页
     *
     * @param commandExecuteSearchDto
     * @return
     */
    @Override
    public DubboResult<CommandExecuteListDto> getCommandExecuteListPage(CommandExecuteSearchDto commandExecuteSearchDto) {
        CommandExecuteQuery commandExecuteQuery = new CommandExecuteQuery();
        commandExecuteQuery.setCommandGroupId(commandExecuteSearchDto.getCommandGroupId());
        commandExecuteQuery.setStartPage(commandExecuteSearchDto.getStartPage());
        commandExecuteQuery.setPageSize(commandExecuteSearchDto.getPageSize());

        PageInfo<CommandExecuteEntity> pageInfo = this.commandService.getCommandExecuteEntityListPage(commandExecuteQuery);

        List<CommandExecuteDto> list = new ArrayList<CommandExecuteDto>();
        for (CommandExecuteEntity commandExecuteEntity : pageInfo.getList()) {
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            this.copyProperty(commandExecuteEntity, commandExecuteDto);
            list.add(commandExecuteDto);
        }

        CommandExecuteListDto commandExecuteListDto = new CommandExecuteListDto();
        commandExecuteListDto.setList(list);
        commandExecuteListDto.setTotalRow((int) pageInfo.getTotal());

        DubboResult<CommandExecuteListDto> dubboResult = new DubboResult<CommandExecuteListDto>();
        dubboResult.setRe(commandExecuteListDto);
        return dubboResult;
    }

    /**
     * 获取模拟器执行命令详情
     *
     * @param commandId
     * @return
     */
    @Override
    public DubboResult<CommandExecuteDto> getCommandExecuteDto(String commandId) {
        DubboResult<CommandExecuteDto> dubboResult = new DubboResult<CommandExecuteDto>();
        try {
            CommandExecuteEntity commandExecuteEntity = this.commandService.getCommandExecuteEntityListByCommandId(commandId);
            CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
            this.copyProperty(commandExecuteEntity, commandExecuteDto);
            dubboResult.setRe(commandExecuteDto);
        } catch (BusinessException e) {
            dubboResult.setBusinessException(e);
        }
        return dubboResult;
    }

    /**
     * 获取模拟器执行命令组详情
     *
     * @param commandGroupId
     * @return
     */
    @Override
    public DubboResult<CommandGroupDto> getCommandGroupDto(String commandGroupId) {
        DubboResult<CommandGroupDto> dubboResult = new DubboResult<CommandGroupDto>();
        try {
            CommandGroupEntity commandGroupEntity = this.commandService.getCommandGroupEntityByGroupId(commandGroupId);
            CommandGroupDto commandGroupDto = new CommandGroupDto();
            this.copyProperty(commandGroupEntity, commandGroupDto);

            List<CommandExecuteDto> commandExecuteDtos = new ArrayList<CommandExecuteDto>();
            for (CommandExecuteEntity commandExecuteEntity : this.commandService.getCommandExecuteEntityListByGroupId(commandGroupEntity.getCommandGroupId())) {
                CommandExecuteDto commandExecuteDto = new CommandExecuteDto();
                this.copyProperty(commandExecuteEntity, commandExecuteDto);
                commandExecuteDtos.add(commandExecuteDto);
            }
            commandGroupDto.setCommandExecuteDtos(commandExecuteDtos);

            dubboResult.setRe(commandGroupDto);
        } catch (BusinessException e) {
            dubboResult.setBusinessException(e);
        }
        return dubboResult;

    }

    private void copyProperty(CommandExecuteEntity commandExecuteEntity, CommandExecuteDto commandExecuteDto) {
        commandExecuteDto.setCommandGroupId(commandExecuteEntity.getCommandGroupId());
        commandExecuteDto.setCommandId(commandExecuteEntity.getCommandId());
        commandExecuteDto.setCommandLine(commandExecuteEntity.getCommandLine());
        commandExecuteDto.setCommandResult(commandExecuteEntity.getCommandResult());
        commandExecuteDto.setStatus(commandExecuteEntity.getStatus());
        commandExecuteDto.setStartDate(commandExecuteEntity.getStartDate());
        commandExecuteDto.setEndDate(commandExecuteEntity.getEndDate());
    }
}
