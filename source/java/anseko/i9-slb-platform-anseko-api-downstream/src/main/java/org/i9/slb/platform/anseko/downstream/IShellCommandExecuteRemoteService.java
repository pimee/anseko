package org.i9.slb.platform.anseko.downstream;

import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;

import java.util.List;

/**
 * shell命令远程服务调用
 *
 * @author R12
 * @date 2018.08.29
 */
public interface IShellCommandExecuteRemoteService {

    void shellCommandExecute(String clientId, ShellCommandParamDto shellCommandParamDto);

    void shellCommandExecuteBatch(String clientId, List<ShellCommandParamDto> shellCommandParamDtos);

}
