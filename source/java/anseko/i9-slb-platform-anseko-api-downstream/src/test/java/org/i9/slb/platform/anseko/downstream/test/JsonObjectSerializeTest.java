package org.i9.slb.platform.anseko.downstream.test;

import com.alibaba.fastjson.JSONObject;
import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;
import org.junit.Test;

import java.util.UUID;

/**
 * JSONObject序列化
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/29 14:55
 */
public class JsonObjectSerializeTest {

    @Test
    public void testSerialize() {
        FileCommandParamDto fileCommandParamDto = new FileCommandParamDto();
        fileCommandParamDto.setCommandId(UUID.randomUUID().toString());
        fileCommandParamDto.setFileContent("aaaaaaaaaaaaaaaaaaaaa");
        System.out.println(JSONObject.toJSONString(fileCommandParamDto));
    }
}
