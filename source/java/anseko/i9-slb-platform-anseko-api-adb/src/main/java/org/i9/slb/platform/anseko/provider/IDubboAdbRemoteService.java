package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.AdbHostDto;

import java.util.List;

/**
 * adb处理远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:48
 */
public interface IDubboAdbRemoteService {
    /**
     * 启动adb服务
     */
    void startAdbServer();

    /**
     * 关闭adb服务
     */
    void killAdbServer();

    /**
     * 连接adb客户端
     *
     * @param adbHostDto
     */
    void connectAdbHost(AdbHostDto adbHostDto);

    /**
     * 批量连接adb客户端
     *
     * @param adbHostDtos
     */
    void batchConnectAdbHost(List<AdbHostDto> adbHostDtos);

    /**
     * 检查adb客户端状态
     *
     * @return
     */
    boolean checkAdbHostConnectStatus(AdbHostDto adbHostDto);

    /**
     * 获取所有adb客户端状态
     *
     * @return
     */
    List<AdbHostDto> getAdbHostConnectStatus();
}
