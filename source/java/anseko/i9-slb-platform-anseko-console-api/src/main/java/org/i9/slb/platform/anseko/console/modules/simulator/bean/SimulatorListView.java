package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import java.util.List;

public class SimulatorListView implements java.io.Serializable {

    private static final long serialVersionUID = 2896017826770126485L;

    private List<SimulatorInfoView> list;

    private int totalRow;

    public List<SimulatorInfoView> getList() {
        return list;
    }

    public void setList(List<SimulatorInfoView> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
