package org.i9.slb.platform.anseko.console.modules.user.bean;

import org.i9.slb.platform.anseko.provider.dto.UserDto;

/**
 * 用户信息视图
 *
 * @author jiangtao
 * @date time
 */
public class UserInfoView implements java.io.Serializable {

    private static final long serialVersionUID = -8647397579090614803L;

    public void copyProperty(UserDto userDto) {
        this.id = userDto.getId();
        this.username = userDto.getUsername();
        this.nickname = userDto.getNickname();
        this.portraitUrl = userDto.getPortraitUrl();
        this.createDate = userDto.getCreateDate();
        this.updateDate = userDto.getUpdateDate();
    }

    /**
     * id
     */
    private String id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 头像
     */
    private String portraitUrl;

    private String createDate;

    private String updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPortraitUrl() {
        return portraitUrl;
    }

    public void setPortraitUrl(String portraitUrl) {
        this.portraitUrl = portraitUrl;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
