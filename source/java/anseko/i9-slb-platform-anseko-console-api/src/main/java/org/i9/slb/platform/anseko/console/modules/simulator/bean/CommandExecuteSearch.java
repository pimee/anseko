package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.console.utils.BaseSearch;

/**
 * 模拟器命令执行搜索
 *
 * @author r12
 * @date 2019年2月27日 13:52:41
 */
public class CommandExecuteSearch extends BaseSearch implements java.io.Serializable {

    private static final long serialVersionUID = 5393898140996932271L;

    private String commandGroupId;

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }
}
