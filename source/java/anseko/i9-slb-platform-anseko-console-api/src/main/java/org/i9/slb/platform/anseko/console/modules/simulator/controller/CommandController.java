package org.i9.slb.platform.anseko.console.modules.simulator.controller;

import org.i9.slb.platform.anseko.console.modules.simulator.bean.CommandExecuteInfoView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.CommandExecuteListView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.CommandExecuteSearch;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.CommandGroupInfoView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.CommandGroupListView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.CommandGroupSearch;
import org.i9.slb.platform.anseko.console.modules.simulator.service.CommandService;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 模拟器执行指令日志控制器
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/27 11:44
 */
@Controller
@RequestMapping("/command")
public class CommandController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandController.class);

    @Autowired
    private CommandService commandService;

    /**
     * 获取命令组列表分页
     *
     * @param commandGroupSearch
     * @return
     */
    @RequestMapping(value = "/getCommandGroupListPage.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<CommandGroupListView> getCommandGroupListPage(CommandGroupSearch commandGroupSearch) {
        CommandGroupListView re = this.commandService.getCommandGroupListPage(commandGroupSearch);
        return HttpResponse.ok(re);
    }

    /**
     * 获取命令组详情
     *
     * @param commandGroupId
     * @return
     */
    @RequestMapping(value = "/getCommandGroupInfo/{commandGroupId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<CommandGroupInfoView> getCommandGroupInfo(@RequestParam("commandGroupId") String commandGroupId) {
        CommandGroupInfoView re = this.commandService.getCommandGroupInfo(commandGroupId);
        return HttpResponse.ok(re);
    }

    /**
     * 获取命令执行列表分页
     *
     * @param commandExecuteSearch
     * @return
     */
    @RequestMapping(value = "/getCommandExecuteListPage.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<CommandExecuteListView> getCommandExecuteListPage(CommandExecuteSearch commandExecuteSearch) {
        CommandExecuteListView re = this.commandService.getCommandExecuteListPage(commandExecuteSearch);
        return HttpResponse.ok(re);
    }

    @RequestMapping(value = "/getCommandExecuteList/{commandGroupId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<CommandExecuteInfoView>> getCommandExecuteList(@PathVariable("commandGroupId") String commandGroupId) {
        List<CommandExecuteInfoView> re = this.commandService.getCommandExecuteList(commandGroupId);
        return HttpResponse.ok(re);
    }

    /**
     * 获取命令执行详情
     *
     * @param commandId
     * @return
     */
    @RequestMapping(value = "/getCommandExecuteInfo/{commandId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<CommandExecuteInfoView> getCommandExecuteInfo(@RequestParam("commandId") String commandId) {
        CommandExecuteInfoView re = this.commandService.getCommandExecuteInfo(commandId);
        return HttpResponse.ok(re);
    }

}
