package org.i9.slb.platform.anseko.console.modules.simulator.service;

import org.i9.slb.platform.anseko.console.modules.simulator.bean.*;

import java.util.List;

/**
 * 下行指令service
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/27 13:35
 */
public interface CommandService {
    /**
     * 获取模拟器下所有的命令组列表
     *
     * @param commandGroupSearch
     * @return
     */
    CommandGroupListView getCommandGroupListPage(CommandGroupSearch commandGroupSearch);

    /**
     * 获取模拟器下所有的命令执行列表
     *
     * @param commandExecuteSearch
     * @return
     */
    CommandExecuteListView getCommandExecuteListPage(CommandExecuteSearch commandExecuteSearch);

    /**
     * 获取命令执行详情
     *
     * @param commandId
     * @return
     */
    CommandExecuteInfoView getCommandExecuteInfo(String commandId);

    /**
     * 获取命令组详情
     *
     * @param commandGroupId
     * @return
     */
    CommandGroupInfoView getCommandGroupInfo(String commandGroupId);

    List<CommandExecuteInfoView> getCommandExecuteList(String commandGroupId);
}
