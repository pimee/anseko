package org.i9.slb.platform.anseko.console.modules.simulator.bean;

public class SimulatorSaveForm implements java.io.Serializable {

    private static final long serialVersionUID = -6698480226367787505L;
    /**
     * 模拟器名称
     */
    private String simulatorName;
    /**
     * cpu核数
     */
    private Integer cpunum;
    /**
     * 内存
     */
    private Integer ramnum;
    /**
     * 安卓版本号
     */
    private Integer androidVersion;
    /**
     * 实例编号
     */
    private String instanceId;

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    public Integer getCpunum() {
        return cpunum;
    }

    public void setCpunum(Integer cpunum) {
        this.cpunum = cpunum;
    }

    public Integer getRamnum() {
        return ramnum;
    }

    public void setRamnum(Integer ramnum) {
        this.ramnum = ramnum;
    }

    public Integer getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(Integer androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
}
