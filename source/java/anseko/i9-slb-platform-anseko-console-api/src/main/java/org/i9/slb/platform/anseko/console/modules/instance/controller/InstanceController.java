package org.i9.slb.platform.anseko.console.modules.instance.controller;

import org.i9.slb.platform.anseko.common.types.VirtualEnum;
import org.i9.slb.platform.anseko.console.modules.common.bean.EnumView;
import org.i9.slb.platform.anseko.console.modules.instance.bean.*;
import org.i9.slb.platform.anseko.console.modules.instance.service.InstanceService;
import org.i9.slb.platform.anseko.console.utils.HttpResponse;
import org.i9.slb.platform.anseko.console.utils.HttpResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 实例控制器
 *
 * @author R12
 * @date 2018.08.30
 */
@Controller
@RequestMapping("/instance")
public class InstanceController {

    @Autowired
    private InstanceService instanceService;

    private static final Logger LOGGER = LoggerFactory.getLogger(InstanceController.class);

    /**
     * 获取模拟器列表
     *
     * @return
     */
    @RequestMapping(value = "/getInstanceList.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<InstanceInfoView>> getInstanceList() {
        List<InstanceInfoView> re = this.instanceService.getInstanceList();
        return HttpResponse.ok(re);
    }

    /**
     * 获取模拟器列表
     *
     * @return
     */
    @RequestMapping(value = "/getInstanceListPage.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<InstanceListView> getInstanceListPage(InstanceSearch instanceSearch) {
        InstanceListView re = this.instanceService.getInstanceListPage(instanceSearch);
        return HttpResponse.ok(re);
    }

    /**
     * 获取模拟器信息
     *
     * @param instanceId
     * @return
     */
    @RequestMapping(value = "/getInstanceInfo/{instanceId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<InstanceInfoView> getInstanceInfo(@PathVariable("instanceId") String instanceId) {
        InstanceInfoView re = this.instanceService.getInstanceInfo(instanceId);
        return HttpResponse.ok(re);
    }

    /**
     * 创建实例信息
     *
     * @param instanceSaveForm
     * @return
     */
    @RequestMapping(value = "/createInstanceInfo.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> createInstanceInfo(InstanceSaveForm instanceSaveForm) {
        this.instanceService.createInstanceInfo(instanceSaveForm);
        return HttpResponse.ok();
    }

    /**
     * 更新实例信息
     *
     * @param instanceUpdateForm
     * @return
     */
    @RequestMapping(value = "/updateInstanceInfo.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> updateInstanceInfo(InstanceUpdateForm instanceUpdateForm) {
        this.instanceService.updateInstanceInfo(instanceUpdateForm);
        return HttpResponse.ok();
    }

    @RequestMapping(value = "/updateInstanceStatus/{instanceId}.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<?> updateInstanceStatus(@PathVariable("instanceId") String instanceId, @RequestParam("status") Integer status) {
        this.instanceService.updateInstanceStatus(instanceId, status);
        return HttpResponse.ok();
    }

    @RequestMapping(value = "/getInstanceListOnline.do_", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult<List<InstanceInfoView>> getOnlineInstanceList() {
        List<InstanceInfoView> re = this.instanceService.getIstanceListOnline();
        return HttpResponse.ok(re);
    }
}
